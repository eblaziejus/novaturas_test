<?php
/***********************************************/
require("includes/db_connection.php");
/***********************************************/
if(isset($_GET['id'])){
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM airport  WHERE id = $id";
	$result = $conn->query($sql);
	if($row = $result->fetch_assoc()) {
		$name  = $row["name"];
		$lat  = $row["lat"];
		$lng  = $row["lng"];
		$country_id  = $row["country_id"];
		$airline_id  = $row["airline_id"];
	}else{
		echo "no airport";
	}
}
include("includes/head.html");
?>
<!-- top menu ----------------------------------------------->
<ul class="nav nav-tabs">
    <li class="active"><a href="index.php">Oro uostai</a></li>
    <li><a href="airlines.php">Avialinijos</a></li>
    <li><a href="countries.php">Šalys</a></li>
</ul>
<!----------------------------------------------------------->
<div class="container">
<script>
function initMap() {
	var map;
	var marker;
	var defaultPosition = {lat: <?php echo $lat ?>, lng: <?php echo $lng ?>};

	map = new google.maps.Map(document.getElementById('map'), {
	  center: defaultPosition,
	  zoom: 7,
	});

	marker = new google.maps.Marker({
	  position: defaultPosition,
	   map: map
	});
	
	var defaultLatLng = marker.getPosition();
	document.getElementById("lat").value = defaultLatLng.lat();
	document.getElementById("lng").value = defaultLatLng.lng();

	google.maps.event.addListener(map, 'click', function(event) {
	  if (marker && marker.setMap) {
		marker.setMap(null);
	  }
	  
	  marker = new google.maps.Marker({
		position: event.latLng,
		map: map
	  });
	  
	  var latlng = marker.getPosition();
	  document.getElementById("lat").value = latlng.lat();
	  document.getElementById("lng").value = latlng.lng();
	});
}
</script>
<script async defer    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3VuEsGP6EKB_p2e6ihWiEYrlofV2rdbk&callback=initMap"></script>
	<div id="map"></div>
	<div>
		<?php
		/****************alerts***************************************/
		if(isset($_GET['msg'])&&($_GET['msg']=='notvalid')){
			echo  "<div class='alert alert-danger margin-left-right' role='alert'><strong>Blogai įvesta!</strong> Pavadinimą gali sudaryti tik raidės ir skaitmenys iki 60 simolių <a href='edit-airport.php?id=$id' class='alert-link'>X</a></div>";
		}elseif(isset($_GET['msg'])&&($_GET['msg']=='empty')){
			echo  "<div class='alert alert-warning margin-left-right' role='alert'><strong>Nieko neįvesta!</strong> Pavadinimas turi būti sudarytas iš raidžių ir skaitmenų iki 60 simolių <a href='edit-airport.php?id=$id' class='alert-link'>X</a></div>";
		}
		/**********************form*************************************/?>
	</div>
	<h4 class="margin-left">Žemėlapyje parinkite lokaciją (ilguma, platuma):</h4>
    <form action="update-airport.php?id=<?php echo $id ?>" method="post">
		<fieldset>
			<div class="col-xs-6"><input type="text" id="lat" name="lat" readonly class="form-control" value="<?php echo $lat ?>"></div>
			<div class="col-xs-6"><input type="text" id="lng" name="lng" readonly class="form-control" value="<?php echo $lng ?>"></div>
			<div class="col-xs-12"><label>Pakeiskite pavadinimą:</label><input type="text" id="name" name="name" class="form-control" value="<?php echo $name ?>"/></div>
			<div class="col-xs-6">
				<label for="select">Pasirinkite šalį:</label>
				<select id="country_id" name="country_id" class="form-control">  
				<?php //require("db_connection.php");
				$sql = "SELECT * FROM country";
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
						if ($row['id']==$country_id){
							echo "<option value=".$row['id']." selected >".$row['countryname']."</option>";	
						}else{
							echo "<option value=".$row['id']." >".$row['countryname']."</option>";	
						}
					}
				}?>	  
				</select> 
			</div>
			<div class="col-xs-6">
				<label for="select">Pasirinkite oro linijas:</label>
				<select id="id" name="id" class="form-control">  
				<?php $sql = "SELECT * FROM airline";
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
						if ($row['id']==$airline_id){
							echo "<option value=".$row['id']." selected >".$row['name']."</option>";	
						}else{	
							echo "<option value=".$row['id']." >".$row['name']."</option>";
						}
					}
				}
			$conn->close();?>	  
				</select> 
			</div>
			<div class="col-xs-12">
				<button type="submit"  class="btn btn-success col-xs-10 margin-top">Išsaugoti</button> 
				<a href="index.php"><button type="button"  class="btn btn-default margin-top margin-left">Grįžti atgal</button></a>
			</div>
		</fieldset>
    </form>
</div>
<?php include("includes/footer.html");?>