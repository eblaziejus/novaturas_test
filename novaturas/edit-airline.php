<?php	include("includes/head.html");?>
<!-- top menu ----------------------------------------------->
<ul class="nav nav-tabs">
    <li><a href="index.php">Oro uostai</a></li>
    <li class="active"><a href="airlines.php">Avialinijos</a></li>
    <li><a href="countries.php">Šalys</a></li>
</ul>
<!----------------------------------------------------------->
<?php
/***********************************************/
require("includes/db_connection.php");
/***********************************************/
if(isset($_GET['id'])){
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM airline  WHERE id = $id";
	$result = $conn->query($sql);
	if($row = $result->fetch_assoc()) {
		$name  = $row["name"];
		$country_id  = $row["country_id"];
	}else{
		echo "no airport";
	}
}
$conn->close();
?>
<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>From Info Windows to a Database: Saving User-Added Form Data</title>
  </head>
  <body>
  <div class="container">
  <?php
  /****************alerts***************************************/
		if(isset($_GET['msg'])&&($_GET['msg']=='notvalid')){
			echo  "<div class='alert alert-danger' role='alert'><strong>Blogai įvesta!</strong> Pavadinimą gali sudaryti tik raidės ir skaitmenys iki 30 simolių <a href='edit-airline.php?id=$id' class='alert-link'>X</a></div>";
		}elseif(isset($_GET['msg'])&&($_GET['msg']=='empty')){
			echo "<div class='alert alert-warning' role='alert'><strong>Nieko neįvesta!</strong> Pavadinimas turi būti sudarytas iš raidžių ir skaitmenų iki 30 simolių <a href='edit-airline.php?id=$id' class='alert-link'>X</a></div>";
		}
  /**********************form*************************************/?>
	<form  action="update-airline.php?id=<?php echo $id;?>" method="post"><!--class="form-inline"-->
      <fieldset>
        <div class="form-group">
          <label for="name">Pavadinimas</label>
          <input type="text" id="name" name="name" class="form-control" value="<?php echo $name; ?>">
        </div>
        <div class="form-group">
          <label for="select">Šalis</label>
		<select id="country_id" name="country_id" class="form-control">  
		<?php require("includes/db_connection.php");
			$sql = "SELECT * FROM country";
			$result = $conn->query($sql);
			
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
						if ($row['id']==$country_id){
							echo "<option value=".$row['id']." selected >".$row['countryname']."</option>";	
						}else{
						echo "<option value=".$row['id']." >".$row['countryname']."</option>";	
						}
					
				}
			}
		$conn->close();?>	  
	  </select>  
        </div>
        <button type="submit"  class="btn btn-success">Išsaugoti pakeitimus</button> 
		<a href="http://localhost/novaturas/airlines"><button type="button"  class="btn btn-default">Grįžti atgal</button></a>
      </fieldset>
    </form>
	<div>
	<?php 
	include("includes/footer.html");
	?>