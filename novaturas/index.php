<?php include("includes/head.html");?>
<!-- top menu ----------------------------------------------->
<ul class="nav nav-tabs">
    <li class="active"><a href="index.php">Oro uostai</a></li>
    <li><a href="airlines.php">Avialinijos</a></li>
    <li><a href="countries.php">Šalys</a></li>
</ul>
<!--------------------------------------------------------->
<div class="container">
	<?php
	require("includes/db_connection.php");
	$sql = "SELECT airport.id, airport.name, airport.lat, airport.lng, country.countryname, airline.name
			FROM airport 
			LEFT JOIN country ON airport.country_id = country.id 
			LEFT JOIN airline ON airport.airline_id = airline.id";
	$result = $conn->query($sql);
	$conn->close();
	/**************************datatable**********************************/
	echo "<table class='table table-bordered'>
	<thead><tr><th>#</th><th>ORO UOSTAS</th><th colspan='2'>LOKACIJA (ilguma,platuma)</th><th>ŠALIS</th><th>ORO LINIJOS</th><th colspan='2'>VEIKSMAI</th></tr></thead><tbody>";
	if ($result->num_rows > 0) {
		$i = 1;
		while($row = $result->fetch_row()) {
		echo "<tr><td scope='row'>".$i++."</td>
				<td>".$row[1]."</td>
				<td>".$row[2]."</td>
				<td>".$row[3]."</td>
				<td>".$row[4]."</td>";
				if($row[5]!=null) {echo "<td>".$row[5]."</td>";}else{echo "<td class='warning'> Nepriskirta </td>";}?>
				<td><a href="edit-airport.php?id=<?php echo $row[0];?>"><button type="button" class="btn btn-info">Redaguoti</button></a></td>
		<td><a href="delete-airport.php?id=<?php echo $row[0];?>" onclick="return confirm('Ar tikrai norite pašalinti įrašą?');"><button type="button" class="btn btn-danger">Šalinti</button></a></td></tr>
	<?php }
	}
	echo "</table></tbody>";
	/****************alerts***************************************/
	if(isset($_GET['msg'])&&($_GET['msg']=='valid')){
		echo  "<div class='alert alert-success margin-bottom' role='alert'><strong>Įrašas sėkmingai išsaugotas</strong><a href='index.php' class='alert-link'>X</a></div>";
	}elseif(isset($_GET['msg'])&&($_GET['msg']=='deleted')){
		echo  "<div class='alert alert-success' role='alert'><strong>Įrašas sėkmingai pašalintas</strong><a href='index.php' class='alert-link'>X</a></div>";
	}
	?>	
	<a href="new-airport.php"> <button  type="button" class="btn btn-block btn-primary">Naujas oro uostas</button> </a>
</div> <!-- /container -->
<?php include("includes/footer.html");?>