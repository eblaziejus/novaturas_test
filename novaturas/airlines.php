<?php include("includes/head.html"); ?>
	<!-- top menu ----------------------------------------------->
	<ul class="nav nav-tabs">
        <li><a href="index.php">Oro uostai</a></li>
        <li class="active"><a href="airlines.php">Avialinijos</a></li>
        <li><a href="countries.php">Šalys</a></li>
    </ul>
	<!----------------------------------------------------------->
<div class="container">
	<?php
	require("includes/db_connection.php");
	$sql ="SELECT airline.id, airline.name, country.countryname FROM airline INNER JOIN country ON airline.country_id = country.id";
	$result = $conn->query($sql);
		echo"<table class='table table-bordered'><thead><th>#</th><th>Avialinijos</th><th>Šalis</th><th colspan='2'>Veiksmai</th></thead><tbody>";
		if ($result->num_rows > 0) {
		$i = 1;
		/**************************datatable**********************************/
		while($row = $result->fetch_assoc()) {
		echo "<tr><td>".$i++."</td>
			  <td>".$row["name"]."</td>
			  <td>".$row["countryname"]."</td>";?>
			  <td><a href="edit-airline.php?id=<?php echo $row['id'];?>"><button type="button" class="btn btn-info">Redaguoti</button></a></td>
			  <td><a href="delete-airline.php?id=<?php echo $row['id'];?>" onclick="return confirm('Ar tikrai norite pašalinti įrašą?');"> <button type="button" class="btn btn-danger">Šalinti</button></a></td></tr>
		<?php
		}
	}	echo "</tbody></table>";
		/****************alerts***************************************/
		if(isset($_GET['msg'])&&($_GET['msg']=='valid')){
			echo  "<div class='alert alert-success' role='alert'><strong>Įrašas sėkmingai išsaugotas</strong><a href='airlines.php' class='alert-link'>X</a></div>";
		}elseif(isset($_GET['msg'])&&($_GET['msg']=='deleted')){
			echo  "<div class='alert alert-success' role='alert'><strong>Įrašas sėkmingai pašalintas</strong><a href='airlines.php' class='alert-link'>X</a></div>";
		}elseif(isset($_GET['msg'])&&($_GET['msg']=='notvalid')){
			echo  "<div class='alert alert-danger' role='alert'><strong>Blogai įvesta!</strong> Pavadinimą gali sudaryti tik raidės ir skaitmenys iki 30 simolių <a href='airlines.php' class='alert-link'>X</a></div>";
		}elseif(isset($_GET['msg'])&&($_GET['msg']=='empty')){
			echo "<div class='alert alert-warning' role='alert'><strong>Nieko neįvesta!</strong> Pavadinimas turi būti sudarytas iš raidžių ir skaitmenų iki 30 simolių <a href='airlines.php' class='alert-link'>X</a></div>";
		}
		/**********************form*************************************/
		?>
	<h4><b>Avialinijų įvedimas</b></h4>
	<form  action="create-airline.php" method="post"><!--class="form-inline"-->
      <fieldset>
        <div class="form-group">
          <label for="name">Įveskite pavadinimą:</label>
          <input type="text" id="name" name="name" class="form-control" placeholder="Pavadinimą gali sudaryti tik raidės ir skaitmenys iki 30 simbolių">
        </div>
			<div class="form-group">
			  <label for="select">Pasirinkite šalį:</label>
			  <select id="country_id" name="country_id" class="form-control" >
				<?php 
				$sql = "SELECT * FROM country";
				$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
						echo "<option value=".$row['id']." >".$row['countryname']."</option>";
					}
				}
				$conn->close();?>
				</select>	 
			</div>
        <button type="submit"  class="btn btn-success">Išsaugoti</button>
      </fieldset>
    </form>
</div> <!-- /container -->
<?php include("includes/footer.html");?>