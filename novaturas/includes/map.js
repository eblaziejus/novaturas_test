function initMap() {
	var map;
	var marker;
	var defaultPosition = {lat: 54.967072, lng: 24.073448};

	map = new google.maps.Map(document.getElementById('map'), {
	  center: defaultPosition,
	  zoom: 7,
	});

	marker = new google.maps.Marker({
	  position: defaultPosition,
	   map: map
	});
	
	var defaultLatLng = marker.getPosition();
	document.getElementById("lat").value = defaultLatLng.lat();
	document.getElementById("lng").value = defaultLatLng.lng();

	google.maps.event.addListener(map, 'click', function(event) {
	  if (marker && marker.setMap) {
		marker.setMap(null);
	  }
	  
	  marker = new google.maps.Marker({
		position: event.latLng,
		map: map
	  });
	  
	  var latlng = marker.getPosition();
	  document.getElementById("lat").value = latlng.lat();
	  document.getElementById("lng").value = latlng.lng();
	});
}
