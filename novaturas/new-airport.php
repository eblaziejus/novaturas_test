<?php include("includes/head.html");?>
<!-- top menu ----------------------------------------------->
<ul class="nav nav-tabs">
    <li class="active"><a href="/">Oro uostai</a></li>
    <li><a href="airlines.php">Avialinijos</a></li>
    <li><a href="countries.php">Šalys</a></li>
</ul>
<!----------------------------------------------------------->
<div class="container">
	<h4 class="margin-left">Naujo oro uosto įvedimas</h4>
	<script src="includes/map.js"></script>
	<script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3VuEsGP6EKB_p2e6ihWiEYrlofV2rdbk&callback=initMap"></script>
	<div id="map"></div>
		<?php
		/****************alerts***************************************/
		if(isset($_GET['msg'])&&($_GET['msg']=='notvalid')){
			echo  "<div class='alert alert-danger margin-left-right' role='alert'><strong>Blogai įvesta!</strong> Pavadinimą gali sudaryti tik raidės ir skaitmenys iki 60 simolių <a href='new-airport.php' class='alert-link'>X</a></div>";
		}elseif(isset($_GET['msg'])&&($_GET['msg']=='empty')){
			echo  "<div class='alert alert-warning margin-left-right' role='alert'><strong>Nieko neįvesta!</strong> Pavadinimas turi būti sudarytas iš raidžių ir skaitmenų iki 60 simolių <a href='new-airport.php' class='alert-link'>X</a></div>";
		}
	/**********************form*************************************/	?>
	<h5 class="margin-left"><b>Žemėlapyje parinkite lokaciją (ilguma, platuma):</b></h5>
	<form action="create-airport.php" method="post">
		<fieldset>
			<div class="col-xs-6"><input type="text" id="lat" name="lat" readonly class="form-control"></div>
			<div class="col-xs-6"><input type="text" id="lng" name="lng" readonly class="form-control"></div>
			<div class="col-xs-12"><label>Įveskite pavadinimą:</label><input type="text" id="name" name="name" class="form-control" placeholder="Įveskite ne ilgesnį kaip 60-ies simblolių pavadinimą"></div>
			<div class="col-xs-6">
				<label for="select">Pasirinkite šalį:</label>
				<select id="country_id" name="country_id" class="form-control">  
				<?php require("includes/db_connection.php");
				$sql = "SELECT * FROM country";
				$result = $conn->query($sql);
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
					echo "<option value=".$row['id']." >".$row['countryname']."</option>";
					}
				}?>	  
				</select> 
			</div>
			<div class="col-xs-6">
				<label for="select">Pasirinkit oro linijas:</label>
				<select id="id" name="id" class="form-control">  
				<?php $sql = "SELECT * FROM airline";
					 $noairlines=0;
					  $result = $conn->query($sql);
						if ($result->num_rows > 0) {
							$noairlines=1;
							while($row = $result->fetch_assoc()) {
							echo "<option value=".$row['id']." >".$row['name']."</option>";
							}
						}
					$conn->close();?>	  
				</select>
			</div>
			<div class="col-xs-12">
			<?php if(!$noairlines) {
					echo "<a href='airlines.php'><button type='button'  class='btn btn-warning margin-top' >Prieš įvesdami oro uostą įveskite bent vieną avialiniją!</button></a>";
				}elseif($noairlines){
					echo "<button type='submit'  class='btn btn-success col-xs-10 margin-top'>Išsaugoti</button>";
				}
			?>
				<a href="index.php"><button type="button"  class="btn btn-default margin-top margin-left">Grįžti atgal</button></a>
			</div>
		</fieldset>
	</form>
</div>
<?php include("includes/footer.html");?>